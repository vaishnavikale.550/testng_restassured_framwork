Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Arohi",
    "job": "qalead"
}

Response header date is : 
Fri, 08 Mar 2024 12:04:33 GMT

Response body is : 
{"name":"Arohi","job":"qalead","id":"839","createdAt":"2024-03-08T12:04:33.310Z"}