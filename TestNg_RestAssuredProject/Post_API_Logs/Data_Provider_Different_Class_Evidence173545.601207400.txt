Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 12:05:45 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"844","createdAt":"2024-03-08T12:05:45.742Z"}