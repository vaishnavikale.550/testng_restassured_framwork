Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 12:05:47 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"605","createdAt":"2024-03-08T12:05:47.806Z"}